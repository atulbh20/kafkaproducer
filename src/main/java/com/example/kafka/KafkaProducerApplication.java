package com.example.kafka;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.annotation.EnableKafka;

@Configuration
@EnableKafka
@SpringBootApplication
public class KafkaProducerApplication implements CommandLineRunner{

	@Autowired
	private KafkaProducer producer;
	
	@Value("${kafka.topic.name}")
    private String topicName;
	
	@Value("${kafka.topic.partitions}")
    private int topicPartitions;
	
	public static void main(String[] args) {
		SpringApplication.run(KafkaProducerApplication.class, args);
	}
	
	@Override
	public void run(String... args) throws IOException {
		List<String> list = new ArrayList<>();
		BufferedReader br = new BufferedReader(new FileReader("C:/Users/Atul/Desktop/input04.txt"));
		list = br.lines().collect(Collectors.toList());
		Iterator<String> lineItr = list.iterator();
		long count = 0L;
		long timest = System.currentTimeMillis();
		while(lineItr.hasNext()){
			String[] s = lineItr.next().split("\\s+");
		    for(int i =0; i< s.length; i++) {
		    	this.producer.sendMessage(topicName, s[i]);
		    	//System.out.println(s[i]);
		        count++;
		    }
		}
		System.out.printf("\ntotal records sent: %s\n", count);
		
	}
}
