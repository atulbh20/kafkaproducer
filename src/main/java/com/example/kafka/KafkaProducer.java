package com.example.kafka;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.util.concurrent.ListenableFutureCallback;

public class KafkaProducer {
	
	@Autowired
	private KafkaTemplate<Integer, String> kafkaTemplate;
	
	private long startTs;
	private long endTs;
	
	
	public void sendMessage(String topic, String message) {
        // the KafkaTemplate provides asynchronous send methods returning a
        // Future
        ListenableFuture<SendResult<Integer, String>> future = kafkaTemplate.send(topic, message);

        // you can register a callback with the listener to receive the result
        // of the send asynchronously
        future.addCallback(
        		new ListenableFutureCallback<SendResult<Integer, String>>() {

                    @Override
                    public void onSuccess(
                    		SendResult<Integer, String> result) {
                    	if("11111".equals(message)){
                    		System.out.println("Started");
                    		startTs = System.currentTimeMillis();
                    	}
                    	if("22222".equals(message)){
                    		endTs = System.currentTimeMillis();
                    		System.out.println("Total time taken to send: " + (endTs - startTs) );
                    	}
                    	
                       
                    	/*System.out.printf("sent message='%s' on partition='%s', with offset=%s\n",
                                message,
                                result.getRecordMetadata().partition(),
                                result.getRecordMetadata().offset()                                
                        		);*/
                    }

                    @Override
                    public void onFailure(Throwable ex) {
                    	System.out.printf("unable to send message='{%s}'",
                                message, ex);
                    }
                });

        // alternatively, to block the sending thread, to await the result,
        // invoke the future’s get() method
    }
}
